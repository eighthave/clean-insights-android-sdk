Clean Insights Android SDK
==========================

This document describes how to get started using the Clean Insights SDK for Android.
 
[CleanInsights](https://cleaninsights.org) gives developers a safe, sustainable, and secure way to
gather insights about their users using cutting edge techniques like differential privacy, 
onion routing, certificate pinning and more.

This project has now moved to Gitlab:
[https://gitlab.com/cleaninsights/clean-insights-android-sdk](https://gitlab.com/cleaninsights/clean-insights-android-sdk)

## Dependency Verification

This library supports dependency verification.
If you run into artifacts failing to verify, you might want to run this command in the project root
to recalculate hashes:

```bash
./gradlew --write-verification-metadata sha256 build pmd
```

Be aware, that you should investigate all changes and make sure, that the updated dependencies don't
contain any unwanted changes!
 