@file:Suppress("unused")

package org.cleaninsights.sdk

import com.squareup.moshi.Json
import java.time.Instant
import java.util.*

enum class Feature {
    @Json(name = "lang")
    Lang,

    @Json(name = "ua")
    Ua
}

open class Consent(val granted: Boolean, val start: Instant = Instant.now(), val end: Instant = Instant.now()) {

    val startDate: Date
        get() = Date.from(start)

    val endDate: Date
        get() = Date.from(end)

    constructor(granted: Boolean, period: Period) : this(granted, period.start, period.end)
}

class FeatureConsent(val feature: Feature, consent: Consent): Consent(consent.granted, consent.start, consent.end)

class CampaignConsent(val campaignId: String, consent: Consent): Consent(consent.granted, consent.start, consent.end)
