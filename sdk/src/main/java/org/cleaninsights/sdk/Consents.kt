package org.cleaninsights.sdk

import java.time.Instant
import kotlin.collections.set

/**
 * This class keeps track of all granted or denied consents of a user.
 *
 * There are two different types of consents:
 * - Consents for common features like if we're allowed to evaluate the locale or a user agent.
 * - Consents per measurement campaign.
 *
 * The time of the consent is recorded along with it's state: If it was actually granted or denied.
 *
 * Consents for common features are given indefinitely, since they are only ever recorded along with
 * running campaigns.
 *
 * Consents for campaigns only last for a certain amount of days.
 */
@Suppress("MemberVisibilityCanBePrivate")
class Consents {

    var features = LinkedHashMap<Feature, Consent>()

    var campaigns = LinkedHashMap<String, Consent>()


    /**
     * User consents to evaluate a `Feature`.
     */
    fun grant(feature: Feature): FeatureConsent {
        // Don't overwrite original grant timestamp.
        if (features[feature] == null || !features[feature]!!.granted) {
            features[feature] = Consent(true)
        }

        return FeatureConsent(feature, features[feature]!!)
    }

    /**
     * User denies consent to evaluate a `Feature`.
     */
    fun deny(feature: Feature): FeatureConsent {
        // Don't overwrite original deny timestamp.
        if (features[feature] == null || features[feature]!!.granted) {
            features[feature] = Consent(false)
        }

        return FeatureConsent(feature, features[feature]!!)
    }

    /**
     * @return if consent to a `Feature` was given.
     */
    fun hasBeenGranted(feature: Feature): Boolean {
        return features[feature]?.granted ?: false
    }

    /**
     * User consents to run a specific campaign.
     *
     * @param campaignId: The campaign ID.
     * @param campaign: The campaign.
     */
    fun grant(campaignId: String, campaign: Campaign): CampaignConsent {

        val period = campaign.nextTotalMeasurementPeriod

        if (period != null) {
            // Always overwrite, since this might be a refreshed consent for a new period.
            campaigns[campaignId] = Consent(true, period)
        }
        else {
            // Consent is technically granted, but has no effect, as start and end
            // will be set the same.
            campaigns[campaignId] = Consent(true)
        }

        return CampaignConsent(campaignId, campaigns[campaignId]!!)
    }

    /**
     * User denies consent to run a specific campaign.
     */
    fun deny(campaignId: String): CampaignConsent {
        // Don't overwrite original deny timestamp.
        if (campaigns[campaignId] == null || campaigns[campaignId]!!.granted) {
            campaigns[campaignId] = Consent(false)
        }

        return CampaignConsent(campaignId, campaigns[campaignId]!!)
    }

    /**
     * @return if consent to run a campaign was given and is valid for the given period.
     */
    fun isCampaignGranted(campaignId: String, start: Instant, end: Instant): Boolean {
        val consent = getConsent(campaignId) ?: return false

        // Is the start of the period inside the consent period? No: -> Discard.
        if (start < consent.start) return false

        // Is the end of the period inside the consent period? No: -> Discard.
        if (end > consent.end) return false

        // Hey! A valid consent.
        return true
    }

    /**
     * @return if consent to run a campaign was given and is now valid.
     */
    fun isCampaignCurrentlyGranted(campaignId: String): Boolean {
        return isCampaignGranted(campaignId, Instant.now(), Instant.now())
    }

    // MARK: Private Methods

    private fun getConsent(campaignId: String): Consent? {
        // Do we have a consent at all? No: -> Discard.
        val consent = campaigns[campaignId] ?: return null

        // Is it actually granted? No: -> Discard.
        if (!consent.granted) return null

        return consent
    }
}
