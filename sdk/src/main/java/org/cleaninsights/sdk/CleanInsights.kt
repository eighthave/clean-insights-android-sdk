package org.cleaninsights.sdk

import android.util.Log
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.io.File
import java.io.IOException
import java.net.HttpURLConnection
import java.time.Instant
import java.util.*
import kotlin.collections.ArrayList

/**
 * @param configuration: The Configuration provided as a `Configuration` object.
 * @param storageDir: The location where to read and persist accumulated data.
 */
@Suppress("unused", "MemberVisibilityCanBePrivate")
open class CleanInsights(val configuration: Configuration, storageDir: File) {

    companion object {
        private const val storageFilename = "cleaninsights.json"
    }

    var conf = configuration
        private set

    private var store = Store()

    private val storageFile = File(storageDir, storageFilename)

    private var persistanceCounter = 0

    private var sending = false

    private val moshi by lazy {
        Moshi.Builder()
                .add(InstantAdapter())
                .add(UrlAdapter())
                .add(FeatureConsentsJsonAdapter())
                .add(CampaignConsentsJsonAdapter())
                .add(VisitListJsonAdapter())
                .add(EventListJsonAdapter())
                .addLast(KotlinJsonAdapterFactory())
                .build()
    }

    init {
        debug("Storage file=%s", storageFile.absolutePath)

        if (storageFile.canRead()) {
            val json = storageFile.readText()

            try {
                val store = moshi.adapter(Store::class.java).fromJson(json)
                if (store != null) this.store = store

                debug("Data loaded from storage file.")
            }
            catch (e: Exception) {
                debug(e)
            }
        }
        else {
            debug("Storage file doesn't exist.")
        }
    }

    /**
     * @param jsonConfiguration: The Configuration provided as a JSON string
     *      which can be deserialized to a `Configuration` object.
     * @param storageDir: The location where to read and persist accumulated data.
     */
    constructor(jsonConfiguration: String, storageDir: File) : this(Configuration(), storageDir) {
        val conf = moshi.adapter(Configuration::class.java).fromJson(jsonConfiguration)

        if (conf != null) {
            this.conf = conf
        }
        else {
            throw IOException("Configuration file could not be read!")
        }
    }

    /**
     * @param jsonConfigurationFile: The Configuration provided as a URL to a JSON file
     *      which can be deserialized to a `Configuration` object.
     * @param storageDir: The location where to read and persist accumulated data.
     */
    constructor(jsonConfigurationFile: File, storageDir: File) : this(jsonConfigurationFile.readText(), storageDir)

    protected fun finalize() {
        // Don't rely on this! When used as a Singleton (which would be typical),
        // This will never be called. Instead, make the app call
        // #persist explicitly on your most important Activity's `#onStop` or on
        // `Application#onTrimMemory`, which has a good indicator of how likely it is, the app
        // will soon be terminated.
        persist()
    }

    /**
     * Track a scene visit.
     *
     * @param scenePath: A hierarchical path best describing the structure of your scenes.
     *      E.g. `['Main', 'Settings', 'Some Setting']`.
     * @param campaignId: The campaign ID as per your configuration, where this measurement belongs to.
     */
    fun measureVisit(scenePath: List<String>, campaignId: String) {
        val campaign = getCampaignIfGood(campaignId, scenePath.joinToString("/")) ?: return

        var visit = getAndMeasure(store.visits, campaignId, campaign) { it.scenePath == scenePath }

        if (visit != null) {
            debug("Gain visit insight: %s", visit)
        }
        else {
            // Align first and last timestamps with campaign measurement period,
            // in order not to accidentally leak more information than promised.
            val period = campaign.currentMeasurementPeriod

            if (period == null) {
                debug("campaign.currentMeasurementPeriod == null! This should not happen!")
                return
            }

            visit = Visit(scenePath, campaignId, period)
            store.visits.add(visit)

            debug("Gain visit insight: %s", visit)
        }

        persistAndSend()
    }

    /**
     * Track an event.
     *
     * @param category: The event category. Must not be empty. (eg. Videos, Music, Games...)
     * @param action: The event action. Must not be empty. (eg. Play, Pause, Duration, Add Playlist, Downloaded, Clicked...)
     * @param campaignId: The campaign ID as per your configuration, where this measurement belongs to.
     * @param name: The event name. OPTIONAL.
     * @param value: The event value. OPTIONAL.
     */
    fun measureEvent(category: String, action: String, campaignId: String, name: String? = null, value: Double? = null) {
        val campaign = getCampaignIfGood(campaignId, "$category/$action") ?: return

        var event = getAndMeasure(store.events, campaignId, campaign) { it.category == category && it.action == action && it.name == name }

        if (event != null) {
            campaign.apply(value, event)

            debug("Gain event insight: %s", event)
        }
        else {
            // Align first and last timestamps with campaign measurement period,
            // in order not to accidentally leak more information than promised.
            val period = campaign.currentMeasurementPeriod

            if (period == null) {
                debug("campaign.currentMeasurementPeriod == null! This should not happen!")
                return
            }

            event = Event(category, action, name, value, campaignId, period)
            store.events.add(event)

            debug("Gain event insight: %s", event)
        }

        persistAndSend()
    }

    val featureSize: Int
        get() = store.consents.features.size

    val campaignSize: Int
        get() = store.consents.campaigns.size

    fun getFeatureConsentByIndex(index: Int): FeatureConsent? {
        val features = store.consents.features

        return try {
            FeatureConsent(
                    ArrayList(features.keys)[index],
                    ArrayList(features.values)[index])
        }
        catch (e: IndexOutOfBoundsException) {
            null
        }
    }

    fun getCampaignConsentByIndex(index: Int): CampaignConsent? {
        val campaigns = store.consents.campaigns

        return try {
            CampaignConsent(
                    ArrayList(campaigns.keys)[index],
                    ArrayList(campaigns.values)[index])
        }
        catch (e: IndexOutOfBoundsException) {
            null
        }
    }

    fun grant(feature: Feature): FeatureConsent {
        val consent = store.consents.grant(feature)

        persistAndSend()

        return consent
    }

    fun deny(feature: Feature): FeatureConsent {
        val consent = store.consents.deny(feature)

        persistAndSend()

        return consent
    }

    fun grant(campaignId: String): CampaignConsent? {
        val campaign = conf.campaigns[campaignId] ?: return null

        val consent = store.consents.grant(campaignId, campaign)

        persistAndSend()

        return consent
    }

    fun deny(campaignId: String): CampaignConsent? {
        if (conf.campaigns[campaignId] == null) return null

        val consent = store.consents.deny(campaignId)

        persistAndSend()

        return consent
    }

    fun isCampaignCurrentlyGranted(campaignId: String): Boolean {
        return store.consents.isCampaignCurrentlyGranted(campaignId)
    }

    fun requestConsent(campaignId: String, consentRequestUi: ConsentRequestUi,
                       completed: ConsentRequestUiComplete? = null) {

        val campaign = conf.campaigns[campaignId]

        if (campaign == null) {
            debug("Cannot request consent: Campaign '%s' not configured.", campaignId)
            if (completed != null) completed(false)
            return
        }

        if (Instant.now() >= campaign.end) {
            debug("Cannot request consent: End of campaign '%s' reached.", campaignId)
            if (completed != null) completed(false)
            return
        }

        if (campaign.nextTotalMeasurementPeriod == null) {
            debug("Cannot request consent: Campaign '%s' configuration seems messed up.", campaignId)
            if (completed != null) completed(false)
            return
        }

        val consent = store.consents.campaigns[campaignId]

        if (consent != null) {
            debug("Already asked for consent for campaign '%s'. It was %s.",
                    campaignId,
                    if (consent.granted) String.format("granted between %s and %s", consent.start, consent.end)
                    else String.format("denied on %s", consent.start))
            if (completed != null) completed(consent.granted)
            return
        }

        val complete = { granted: Boolean ->
            if (granted) {
                store.consents.grant(campaignId, campaign)
            }
            else {
                store.consents.deny(campaignId)
            }

            if (completed != null) completed(granted)
        }

        MainScope().launch {
            consentRequestUi.show(campaignId, campaign, complete)
        }
    }

    fun requestConsent(feature: Feature, consentRequestUi: ConsentRequestUi,
                       completed: ConsentRequestUiComplete? = null) {

        val consent = store.consents.features[feature]

        if (consent != null) {
            debug("Already asked for consent for feature '%s'. It was %s on %s.",
                    feature.name,
                    if (consent.granted) "granted" else "denied",
                    consent.start)
            if (completed != null) completed(consent.granted)
            return
        }

        val complete = { granted: Boolean ->
            if (granted) {
                store.consents.grant(feature)
            }
            else {
                store.consents.deny(feature)
            }

            if (completed != null) completed(store.consents.hasBeenGranted(feature))
        }

        MainScope().launch {
            consentRequestUi.show(feature, complete)
        }
    }

    /**
     * Persist accumulated data to the filesystem.
     *
     * The app should call this on `applicationDidEnterBackground:`.
     */
    fun persist() {
        persist(async = false, force = true)
    }


    // MARK: Private Methods

    /**
     * Persist accumulated data to the filesystem.
     *
     * @param async: If true, returns immediately and does persistance asynchronously, only if it's already due.
     * @param force: Write regardless of threshold reached.
     */
    private fun persist(async: Boolean, force: Boolean = false) {

        val work = {
            try {
                val json = moshi.adapter(Store::class.java).toJson(store)

                storageFile.writeText(json)

                persistanceCounter = 0

                debug("Data persisted to storage file.")
            }
            catch (e: Exception) {
                debug(e)
            }
        }

        persistanceCounter += 1

        if (force || persistanceCounter >= conf.persistEveryNTimes) {
            if (async) {
                GlobalScope.launch {
                    work()
                }
            }
            else {
                work()
            }
        }
    }

    /**
     * Persist data asynchronously and send all data to the CleanInsights Matomo Proxy server.
     *
     * If sending was successful, remove sent data from store and persist again.
     */
    private fun persistAndSend() {
        persist(true)

        if (sending) return

        sending = true

        val notSendingAnymore = { e: Exception? ->
            if (e != null) debug(e)

            sending = false
        }

        GlobalScope.launch {
            val insights = Insights(conf, store)

            if (insights.isEmpty) {
                notSendingAnymore(null)
                return@launch
            }

            val body: String

            try {
                body = moshi.adapter(Insights::class.java).toJson(insights)
            }
            catch (e: Exception) {
                notSendingAnymore(e)
                return@launch
            }

            @Suppress("BlockingMethodInNonBlockingContext")
            val conn = conf.server.openConnection() as HttpURLConnection
            conn.useCaches = false
            conn.connectTimeout = (conf.timeout * 1000).toInt()
            @Suppress("BlockingMethodInNonBlockingContext")
            conn.requestMethod = "POST"
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8")

            debug("Send data to server:\n\n%s %s\n%s\n\n%s\n",
                    conn.requestMethod, conn.url, conn.requestProperties, body)

            @Suppress("BlockingMethodInNonBlockingContext")
            conn.outputStream.writer().write(body)
            @Suppress("BlockingMethodInNonBlockingContext")
            conn.outputStream.flush()
            @Suppress("BlockingMethodInNonBlockingContext")
            conn.outputStream.close()

            if (conn.responseCode != 200 && conn.responseCode != 204) {
                debug("HTTP Error %s: %s", conn.responseCode, conn.inputStream.reader().readText())

                notSendingAnymore(null)
                return@launch
            }

            debug("Successfully offloaded data.")

            insights.clean(store)

            persist(async = true, force = true)

            notSendingAnymore(null)
        }
    }

    private fun getCampaignIfGood(campaignId: String, debugString: String): Campaign? {
        val campaign = conf.campaigns[campaignId]

        if (campaign == null) {
            debug("Measurement '%s' discarded, because campaign '%s' is missing in configuration.", debugString, campaignId)
            return null
        }

        val now = Instant.now()

        if (now < campaign.start) {
            debug("Measurement '%s' discarded, because campaign '%s' didn't start, yet.", debugString, campaignId)
            return null
        }

        if (now > campaign.end) {
            debug("Measurement '%s' discarded, because campaign '%s' already ended.", debugString, campaignId)
            return null
        }

        if (!isCampaignCurrentlyGranted(campaignId)) {
            debug("Measurement '%s' discarded, because campaign '%s' has no user consent yet, any more or we're outside the measurement period.",
                    debugString, campaignId)
            return null
        }

        return campaign
    }


    /**
     * Get a `DataPoint` subclass out of the `haystack`, as long as it fits the `campaign`.
     * Increases `times` according to the campaigns rules.
     *
     * Create a new `DataPoint` if nothing is returned here.
     *
     * @param haystack: The haystack full of `DataPoint` subclasses.
     * @param campaignId: The campaign ID it must match.
     * @param campaign: The campaign parameters to match against.
     * @param where: Additional condition for selection.
     * @return a `DataPoint` subclass out of the `haystack`, as long as it fits the `campaign`.
     */
    private fun <T : DataPoint>getAndMeasure(haystack: List<T>, campaignId: String, campaign: Campaign, where: ((T) -> Boolean)): T? {
        val period = campaign.currentMeasurementPeriod

        if (period == null) {
            debug("campaign.currentMeasurementPeriod == null! This should not happen!")
            return null
        }

        try {
            val dataPoint = haystack.first {
                it.campaignId == campaignId
                        && it.first >= period.start
                        && it.first <= period.end
                        && it.last >= period.start
                        && it.last <= period.end
                        && `where`(it)
            }

            if (!campaign.onlyRecordOnce) dataPoint.times += 1

            return dataPoint
        }
        catch (e: NoSuchElementException) {
            return null
        }
    }

    private fun debug(format: String, vararg args: Any?) {
        if (!conf.debug) return

        Log.d("CleanInsightsSDK", String.format(Locale.US, format, *args))
    }

    private fun debug(e: Exception) {
        debug(e.localizedMessage ?: e.toString())
    }
}