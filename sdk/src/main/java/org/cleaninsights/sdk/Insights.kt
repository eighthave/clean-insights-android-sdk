package org.cleaninsights.sdk

import android.os.Build
import java.time.Instant
import java.util.*
import kotlin.collections.ArrayList

/**
 * Create an `Insights` object according to configuration with all data from the store which is due
 * for offloading to the server.
 *
 * @param conf: The current configuration.
 * @param store: The current measurement and consents store.
 */
@Suppress("unused", "MemberVisibilityCanBePrivate")
class Insights(conf: Configuration, store: Store) {

    companion object {
        val userAgent: String
            get() {
                var httpAgent = System.getProperty("http.agent")

                if (httpAgent == null || httpAgent.startsWith("Apache-HttpClient/UNAVAILABLE (java")) {
                    httpAgent = String.format(Locale.US,
                            "Dalvik/%s (Linux; U; Android %s; %s Build/%s)",
                            System.getProperty("java.vm.version") ?: "0.0.0",
                            Build.VERSION.RELEASE, Build.MODEL, Build.ID)
                }

                return httpAgent
            }
    }

    /**
     * Matomo site ID.
     */
    val idsite = conf.siteId

    /**
     * Preferred user languages as a HTTP Accept header.
     */
    val lang = if (store.consents.hasBeenGranted(Feature.Lang)) Locale.getDefault().language else null

    /**
     * User Agent string.
     */
    val ua = if (store.consents.hasBeenGranted(Feature.Ua)) userAgent else null

    /**
     * Visit` data points.
     */
    val visits = ArrayList<Visit>()

    /**
    Event data points.
     */
    val events = ArrayList<Event>()

    init {
        val now = Instant.now()

        for (visit in store.visits) {
            if (!conf.campaigns.containsKey(visit.campaignId)) {
                continue
            }

            // Only send, after aggregation period is over. `last` should contain that date!
            if (now > visit.last) {
                visits.add(visit)
            }
        }

        for (event in store.events) {
            if (!conf.campaigns.containsKey(event.campaignId)) {
                continue
            }

            // Only send, after aggregation period is over. `last` should contain that date!
            if (now > event.last) {
                events.add(event)
            }
        }
    }


    val isEmpty: Boolean
        get() = visits.isEmpty() && events.isEmpty()

    /**
     * Removes all visits and events from the given `Store`, which are also available in here.
     *
     * This should be called, when all `Insights` were offloaded at the server successfully.
     *
     * @param store: The store where the `Visit`s and `Event`s in here came from.
     */
    fun clean(store: Store) {
        store.visits.removeAll(visits)

        store.events.removeAll(events)
    }
}
