package org.cleaninsights.sdk

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.net.URL
import java.time.Instant
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*

internal class UrlAdapter {

    @ToJson
    fun toJson(url: URL): String {
        return url.toString()
    }

    @FromJson
    fun fromJson(json: String): URL {
        return URL(json)
    }
}

internal class InstantAdapter {

    @ToJson
    fun toJson(instant: Instant): String {
        return DateTimeFormatter.ISO_INSTANT.withZone(ZoneId.from(ZoneOffset.UTC)).format(instant)
    }

    @FromJson
    fun fromJson(json: String): Instant {
        return Instant.parse(json)
    }
}

internal class FeatureConsentsJsonAdapter {

    @ToJson
    fun toJson(consents: LinkedHashMap<Feature, Consent>): Map<Feature, Consent> {
        return consents
    }

    @FromJson
    fun fromJson(consents: Map<Feature, Consent>): LinkedHashMap<Feature, Consent> {
        return LinkedHashMap(consents)
    }
}

internal class CampaignConsentsJsonAdapter {

    @ToJson
    fun toJson(consents: LinkedHashMap<String, Consent>): Map<String, Consent> {
        return consents
    }

    @FromJson
    fun fromJson(consents: Map<String, Consent>): LinkedHashMap<String, Consent> {
        return LinkedHashMap(consents)
    }
}

internal class VisitListJsonAdapter {

    @ToJson
    fun toJson(list: ArrayList<Visit>): List<Visit> {
        return list
    }

    @FromJson
    fun fromJson(list: List<Visit>): ArrayList<Visit> {
        return ArrayList(list)
    }
}

internal class EventListJsonAdapter {

    @ToJson
    fun toJson(list: ArrayList<Event>): List<Event> {
        return list
    }

    @FromJson
    fun fromJson(list: List<Event>): ArrayList<Event> {
        return ArrayList(list)
    }
}
