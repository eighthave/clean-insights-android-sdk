package org.cleaninsights.sdk

import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

class Period(val start: Instant, val end: Instant) {

    val startDate: Date
        get() = Date.from(start)

    val endDate: Date
        get() = Date.from(end)
}

/**
 * @param start: The start of the campaign. (inclusive)
 * @param end: The end of the campaign. (inclusive)
 * @param aggregationPeriodLength: The length of the aggregation period in number of days.
 *      At the end of a period, the aggregated data will be sent to the analytics server.
 * @param numberOfPeriods: The number of periods you want to measure in a row. Therefore the total
 *      length in days you measure one user is `aggregationPeriodLength * numberOfPeriods` beginning
 *      with the first day of the next period after the user consented.
 * @param onlyRecordOnce: Will result in recording only the first time a visit or event happened per
 *      period. Useful for yes/no questions.
 * @param eventAggregationRule: The rule how to aggregate the value of an event (if any given) with
 *      subsequent calls.
 */
@Suppress("MemberVisibilityCanBePrivate")
class Campaign(val start: Instant, val end: Instant, val aggregationPeriodLength: Long,
               val numberOfPeriods: Int = 1, val onlyRecordOnce: Boolean = false,
               val eventAggregationRule: EventAggregationRule = EventAggregationRule.Sum) {

    val currentMeasurementPeriod: Period?
        get() {
            if (numberOfPeriods < 1) return null

            var now = Instant.now()

            var periodEnd = start

            do {
                periodEnd = periodEnd.plus(aggregationPeriodLength, ChronoUnit.DAYS)
            } while (periodEnd <= now)

            var periodStart = periodEnd.minus(aggregationPeriodLength, ChronoUnit.DAYS)

            if (periodStart < start) periodStart = start

            if (end < periodEnd) periodEnd = end

            now = Instant.now()

            if (periodStart > now || periodEnd < now) {
                return null
            }

            return Period(periodStart, periodEnd)
        }

    val nextTotalMeasurementPeriod: Period?
        get() {
            val current = currentMeasurementPeriod ?: return null

            val periodStart = current.end

            var periodEnd = periodStart

            var counter = 0

            while (counter < numberOfPeriods) {
                periodEnd = periodEnd.plus(aggregationPeriodLength, ChronoUnit.DAYS)

                if (periodEnd > end) {
                    periodEnd = periodEnd.minus(aggregationPeriodLength, ChronoUnit.DAYS)
                    break
                }

                counter++
            }

            if (periodStart == periodEnd) {
                return null
            }

            return Period(periodStart, periodEnd)
        }

    override fun toString(): String {
        return String.format("[%s: start=%s, end=%s, aggregationPeriodLength=%d, numberOfPeriods=%d, onlyRecordOnce=%b, eventAggregationRule=%s]",
                this::class, start, end, aggregationPeriodLength, numberOfPeriods, onlyRecordOnce, eventAggregationRule)
    }

    /**
     * Apply the `eventAggregationRule` to the given event with the given value.
     *
     * @param value: The value to apply.
     * @param event: The event to apply the value to.
     */
    fun apply(value: Double?, event: Event) {
        if (value == null || onlyRecordOnce) return

        val oldVal = event.value ?: 0.0

        when (eventAggregationRule) {
            EventAggregationRule.Sum ->
                event.value = oldVal + value

            EventAggregationRule.Avg ->
                event.value = (oldVal * (event.times - 1) + value) / event.times
        }
    }
}