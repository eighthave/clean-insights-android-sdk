package org.cleaninsights.sdk

import java.net.URL

/**
 * @param server: The server URL should look like `https://myhost.example.com/ci/cleaninsights.php`.
 * @param siteId: The Matomo site ID to record this data for.
 * @param timeout: Connection timeout. OPTIONAL, defaults to 5 seconds.
 * @param debug: When set, CleanInsights SDK will print some debug output to STDOUT. OPTIONAL. Defaults to false.
 * @param campaigns: Campaign configuration.
 * @param persistEveryNTimes: Regulates, how often data persistence is done. OPTIONAL. Defaults to 10.
 *      If set to 1, every time something is tracked, *ALL* data is stored to disk. The more you track,
 *      the higher you should set this to avoid heavy load due to disk I/O.
 */
class Configuration(val server: URL, val siteId: Int, val timeout: Double = 5.0, val debug: Boolean,
                    val campaigns: Map<String, Campaign>, val persistEveryNTimes: Int = 10) {

    internal constructor() : this(URL("http://example.org/"), -1, 5.0, true, HashMap<String, Campaign>())

    override fun toString(): String {
        return String.format("[%s: server=%s, siteId=%d, timeout=%f, debug=%b, campaigns=%s]",
                this::class, server, siteId, timeout, debug, campaigns)
    }
}