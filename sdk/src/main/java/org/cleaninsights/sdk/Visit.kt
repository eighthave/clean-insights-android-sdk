package org.cleaninsights.sdk

import java.time.Instant

/**
 * @param scenePath: A hierarchical path to the scene visited.
 * @param campaignId: The campaign ID this data point is for.
 * @param times: Number of times this data point has arisen between `first` and `last`. OPTIONAL, defaults to 1.
 * @param first: The first time this data point has arisen. OPTIONAL, defaults to now.
 * @param last: The last time this data point has arisen. OPTIONAL, defaults to now.
 */
class Visit(val scenePath: List<String>, campaignId: String, times: Int = 1, first: Instant = Instant.now(), last: Instant = Instant.now()) :
        DataPoint(campaignId, times, first, last) {

    constructor(scenePath: List<String>, campaignId: String, period: Period?)
            : this(scenePath, campaignId, 1, period?.start ?: Instant.now(), period?.end ?: Instant.now())

    override fun toString(): String {
        return String.format("[%s: scenePath=%s, campaignId=%s, times=%d, first=%s, last=%s]",
                this::class, scenePath, campaignId, times, first, last)
    }
}