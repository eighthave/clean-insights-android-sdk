package org.cleaninsights.sdk

import java.time.Instant

/**
 * @param campaignId: The campaign ID this data point is for.
 * @param times: Number of times this data point has arisen between `first` and `last`. OPTIONAL, defaults to 1.
 * @param first: The first time this data point has arisen. OPTIONAL, defaults to now.
 * @param last: The last time this data point has arisen. OPTIONAL, defaults to now.
 */
open class DataPoint(val campaignId: String, var times: Int = 1, val first: Instant = Instant.now(),
                     val last: Instant = Instant.now()) {

    override fun toString(): String {
        return String.format("[%s: campaignId=%s, times=%d, first=%s, last=%s]",
                this::class, campaignId, times, first, last)
    }
}
