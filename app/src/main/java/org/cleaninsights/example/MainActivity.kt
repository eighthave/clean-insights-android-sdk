package org.cleaninsights.example

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import org.cleaninsights.example.databinding.ActivityMainBinding
import org.cleaninsights.sdk.Feature

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private val start = System.currentTimeMillis()
    private var firstTime = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(layoutInflater)

        binding.btConsents.setOnClickListener(this)

        setContentView(binding.root)
    }

    override fun onResume() {
        super.onResume()

        if (firstTime) {
            val ui = ConsentRequestUi(this)

            ExampleApp.cleanInsights.requestConsent("test", ui) { granted ->
                if (!granted) return@requestConsent

                ExampleApp.cleanInsights.requestConsent(Feature.Lang, ui) {
                    ExampleApp.cleanInsights.requestConsent(Feature.Ua, ui)
                }

                val time = (System.currentTimeMillis() - start) / 1000.0

                ExampleApp.cleanInsights.measureEvent("app-state", "startup-success", "test", "time-needed", time)
                ExampleApp.cleanInsights.measureVisit(listOf("Main"), "test")
            }

            firstTime = false
        }
        else {
            ExampleApp.cleanInsights.measureVisit(listOf("Main"), "test")
        }
    }

    override fun onClick(view: View?) {
        startActivity(Intent(this, ConsentsActivity::class.java))
    }
}